<?php
/* @var $this KecamatanController */
/* @var $model Kecamatan */
$this->title ="List Jenis Perizinan";
$this->breadcrumbs=array(
	'Jenis Perizinan'=>array('index'),
);

$this->menu =array(
	'Active' => 'List Jenis Perizinan',
	'List Jenis Perizinan'=>'JenisPerizinan/index',
	'Tambah Jenis Perizinan'=>'JenisPerizinan/tambah',
	);
?>



<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'JenisPerizinan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile'=>false,
	'summaryText' => '',
	'columns'=>array(
		'nama_jenis',
        'singkatan_jenis',
		array(
                'header'=> 'Waktu Proses',
                'name'=>'waktu_proses',
                'value'=>
                    function($data){                        
                            $return = $data->waktu_proses.' Hari';
                        return $return;
                    },
             ),
		array(
                'header'=>'Persyaratan',
                'name'=>'persyaratan',
                'type'=>'html',
                'value'=>'$data->persyaratan',
        ),
		array(         
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>array(
                'update'=>array(
                    'label'=> 'Edit JenisPerizinan',
                    'url'=>'Yii::app()->createUrl("JenisPerizinan/ubah", array("id"=>$data->id_jenisperizinan))',
                ),
                'delete'=>array(
                	'label'=>'Hapus JenisPerizinan',
                	'url'=>'Yii::app()->createUrl("JenisPerizinan/hapus", array("id"=>$data->id_jenisperizinan))',
                ),                
            )
        ),
	),
)); ?>
