<?php
$this->title= "Formulir Jenis Perizinan";
$this->breadcrumbs=array(
    'Data Jenis Perizinan'=>array('JenisPerizinan/index'),
);


?>

<!-- Formulir -->
<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'FormJenisPerizinan',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )
);
echo $form->textFieldGroup($model, 'nama_jenis');
echo $form->textFieldGroup($model, 'singkatan_jenis');
echo $form->textFieldGroup($model,'waktu_proses',array('append'=>'Hari'));
echo $form->html5EditorGroup(
			$model,
			'persyaratan',
			 array(
				'widgetOptions' => array(
					'editorOptions' => array(
						'class' => 'span4',
						'rows' => 5,
						'height' => '200',
						'options' => array('color' => true),
						'language'=>'id',
					),
				)
				)
			);
$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);
 
$this->endWidget();
unset($form);
