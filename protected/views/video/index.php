<?php
/* @var $this KecamatanController */
/* @var $model Kecamatan */
$this->title ="List Video";
$this->breadcrumbs=array(
	'Video'=>array('index'),
);


?>



<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'video-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile'=>false,
	'summaryText' => '',
	'columns'=>array(
		'judul_video',
		array(         
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
            	'view'=>array(
                    'label'=> 'Detail Video',
                    'url'=>'Yii::app()->createUrl("Video/detail", array("id"=>$data->id_video))',
                ),
                'update'=>array(
                    'label'=> 'Edit Video',
                    'url'=>'Yii::app()->createUrl("Video/ubah", array("id"=>$data->id_video))',
                ),
                'delete'=>array(
                	'label'=>'Hapus Video',
                	'url'=>'Yii::app()->createUrl("Video/hapus", array("id"=>$data->id_video))',
                ),                
            )
        ),
	),
)); ?>
