<?php
$this->title= "Formulir Perizinan";
$this->breadcrumbs=array(
    'Data Jenis Perizinan'=>array('Perizinan/index'),
);


?>

<!-- Formulir -->
<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'FormPerizinan',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )
);
echo "<label> Jenis Perizinan </label><br>";
echo Select2::ActiveDropDownList($model,'id_jenisperizinan',Jenisperizinan::model()->getOptions(),array(
                'id'=>'id_jenisperizinan',
                'empty'=>'Pilih Jenis Izin',
                'select2Options'=>array(
                    'width'=>'100%',
                    'placeholder'=>"Pilih Jenis Izin"
                    )
                ));

echo $form->textFieldGroup($model, 'nama_pemohon');
echo $form->textFieldGroup($model, 'alamat');
echo $form->textFieldGroup($model, 'perusahaan');

echo $form->dropDownListGroup(
            $model,
            'status',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => array('1'=>'Selesai','0'=>'Proses'),
                )
            )
        );
$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);
 
$this->endWidget();
unset($form);
