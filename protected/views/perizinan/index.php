<?php
/* @var $this KecamatanController */
/* @var $model Kecamatan */
$this->title ="List Perizinan";
$this->breadcrumbs=array(
	'Perizinan'=>array('index'),
);

$this->menu =array(
	'Active' => 'List Perizinan',
	'List Perizinan'=>'Perizinan/index',
	'Tambah Perizinan'=>'Perizinan/tambah',
	);
?>



<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Perizinan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile'=>false,
	'summaryText' => '',
	'columns'=>array(
		array(
                'header'=>'Jenis Perizinan',
                'name'=>'id_jenisperizinan',
                'value'=>
                    function($data){
                    	if($data->id_jenisperizinan != ''):                       
                       		$return = $data->idJenisperizinan->singkatan_jenis;
                       	else:
                       		$return = '';
                       	endif;

                       return $return;
                    },
                 'type'=>'html',
                 'filter'=>Chtml::activeDropDownList($model, 'id_jenisperizinan',Jenisperizinan::model()->getOptions() ,array('empty'=>'')),                
        ),		
		'nama_pemohon',
        'alamat',
		'perusahaan',
		array(
                'header'=>'Status',
                'name'=>'status',
                'value'=>
                    function($data){
                       if($data->status == '1'):
                       		$return = '<span class="blue">Selesai</span>';
                       else:
                       		$return = '<span class="red">Proses</span>';
                       endif;
                       return $return;
                    },
                 'type'=>'html',
                 'filter'=>Chtml::activeDropDownList($model, 'status',array('0'=>'Proses','1'=>'Selesai') ,array('empty'=>'')),                
        ),		
		array(         
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>array(
                'update'=>array(
                    'label'=> 'Edit Perizinan',
                    'url'=>'Yii::app()->createUrl("Perizinan/ubah", array("id"=>$data->id_perizinan))',
                ),
                'delete'=>array(
                	'label'=>'Hapus Perizinan',
                	'url'=>'Yii::app()->createUrl("Perizinan/hapus", array("id"=>$data->id_perizinan))',
                ),                
            )
        ),
	),
)); ?>
