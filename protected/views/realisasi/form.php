<?php
$this->title= "Formulir Realisasi";
$this->breadcrumbs=array(
    'Data Realisasi'=>array('Realisasi/index'),
);


?>

<!-- Formulir -->
<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'FormRealisasi',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )
);

echo $form->dropDownListGroup(
			$model,
			'kategori',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
	   			'widgetOptions' => array(
	   				'data' => array('0'=>'PMA','1'=>'PMDN'),
				),
				'prepend' => '<i class="glyphicon glyphicon-list-alt"></i>',
			)
		);

echo $form->datePickerGroup(
			$model,
			'tahun',
			array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'id',
						'format'=>'yyyy',
						'startView'=>'decade',
						'minViewMode'=>2,
						'autoclose'=>true,
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
			)
		);

$this->widget('ext.moneymask.MMask',array(
            'element'=>'#Realisasi_jumlah',
            'currency'=>'IDR',
            'config'=>array(
                'showSymbol'=>false,
                'symbolStay'=>false,
                'decimal'=>'.',
                'thousands'=>',',
            )
        ));
  
echo $form->textFieldGroup($model,'jumlah',array('prepend'=>'Rp',));

$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);
 
$this->endWidget();
unset($form);
