<?php
/* @var $this KecamatanController */
/* @var $model Kecamatan */
$this->title ="List Realisasi";
$this->breadcrumbs=array(
	'Realisasi'=>array('index'),
);

$this->menu =array(
	'Active' => 'List Realisasi',
	'List Realisasi'=>'Realisasi/index',
	'Tambah Realisasi'=>'Realisasi/tambah',
	);
?>



<?php
$this->widget(
    'booster.widgets.TbHighCharts',
    array(
        'options' => array(
             'chart'=>array(
            'type'=> 'column'
            ),
            'title' => array(
                'text' => 'Grafik Realisasi Tahunan',
                'x' => -20 //center
            ),
            'xAxis' => array(
                'categories' => $model->getTahun(),
            ),
            'yAxis' => array(
                'title' => array(
                    'text' =>  'Realisasi',
                ),
                'plotLines' => [
                    [
                        'value' => 0,
                        'width' => 1,
                        'color' => '#808080'
                    ]
                ],
   
            ),
            'plotOptions' =>array(
                  'column'=>array(
                    'dataLabels'=>array(
                            'enabled'=>true,
                        )
                  ),  
            ),
            'tooltip' => array(
                'valuePrefix'=>'Rp.',
                'valueSuffix' => '.00'
            ),
            'legend' => array(
                'layout' => 'vertical',
                'align' => 'right',
                'verticalAlign' => 'middle',
                'borderWidth' => 0
            ),            
            'series' => array(
                array(
          
                    'name' => 'PMA',
                    'data' => $model->getPma(),
                ),
                array(
               
                    'name' => 'PMDN',
                    'data' => $model->getPmdn(),
                ),               
            )
        ),
        'htmlOptions' => array(
            'style' => 'min-width: 310px; height: 400px; margin: 0 auto'
        )
    )
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Realisasi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile'=>false,
	'summaryText' => '',
	'columns'=>array(
        array(
                'header'=> 'Kategori',
                'name'=>'kategori',
                'value'=>
                    function($data){
                        if($data->kategori == '0'):
                            $return = "PMA";
                        elseif($data->kategori == "1"):
                            $return = "PMDN";
                        endif;
                        return $return;
                    },
             ),
		'tahun',
        array(
                'header'=>'Jumlah',
                'name'=>'jumlah',
                'value'=>
                    function($data){
                        $return = 'Rp. '.Yii::app()->format->formatNumber($data->jumlah);
                        return $return;
                            },                 
        ),

		array(         
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>array(            	
                'update'=>array(
                    'label'=> 'Edit Realisasi',
                    'url'=>'Yii::app()->createUrl("Realisasi/ubah", array("id"=>$data->id_realisasi))',
                ),
                'delete'=>array(
                	'label'=>'Hapus Realisasi',
                	'url'=>'Yii::app()->createUrl("Realisasi/hapus", array("id"=>$data->id_realisasi))',
                ),                
            )
        ),
	),
)); ?>
