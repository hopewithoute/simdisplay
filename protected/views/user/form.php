<?php
$this->title = "Tambah User";
$this->breadcrumbs=array(
	'Data User'=>array('index'),
);

$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'verticalForm',
        'htmlOptions' => array('class' => 'well'),
         'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ), // for inset effect
    )
);
echo $form->textFieldGroup($model,'username',array('autocomplete'=>'off'));
echo $form->passwordFieldGroup($model,'password',array('autocomplete'=>'off'));
echo $form->passwordFieldGroup($model,'password_repeat');
$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);

$this->endWidget();
unset($form);
