<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'form-Produk',
        'htmlOptions'=>array('class'=>'form-signin'),
    )
);

    
echo $form->textFieldGroup($model,'username',array('labelOptions'=>array('label'=>''),'class'=>'form-control'));
echo $form->passwordFieldGroup($model,'password',array('labelOptions'=>array('label'=>''),'class'=>'form-control'));
echo $form->checkboxGroup($model, 'rememberMe',array('class'=>'pull-left'));

$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit','label' => 'Login','htmlOptions'=>array('class'=>'btn-primary btn-block'))
);
$this->endWidget(); ?>
 
