<?php
$this->title = "Formulir Konfigurasi";
$this->breadcrumbs = array(
    'Konfigurasi' => array('Config/index'),
);


?>

    <!-- Formulir -->
<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'FormKonfigurasi',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )
);

echo "<label> Video Aktif </label><br>";
echo Select2::ActiveDropDownList($model, 'video_aktif', Video::model()->getOptions(), array(
    'id' => 'video_aktif',
    'empty' => 'Pilih Video',

));

echo "<br><hr>";
echo $form->html5EditorGroup(
    $model,
    'text_atas',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 5,
                'height' => '200',
                'options' => array('color' => true),
                'language' => 'id',
            ),
        )
    )
);
echo $form->html5EditorGroup(
    $model,
    'pesan_atas',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 5,
                'height' => '200',
                'options' => array('color' => true),
                'language' => 'id',
            ),
        )
    )
);
echo $form->html5EditorGroup(
    $model,
    'pesan_bawah',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 5,
                'height' => '200',
                'options' => array('color' => true),
                'language' => 'id',
            ),
        )
    )
);
$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);

$this->endWidget();
unset($form);
