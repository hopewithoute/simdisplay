<?php
/* @var $this SiteController */
/* @var $error array */


?>
<div class="container-error">
<?php echo CHtml::link('Kembali Ke Halaman Utama',array('Site/Index')); ?>
<hr>

<h2>Error <?php echo $code; ?></h2>
<?php echo CHtml::encode($message); ?>

</div>