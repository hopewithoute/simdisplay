<?php
$this->title= "Formulir Galeri";
$this->breadcrumbs=array(
    'Data Galeri'=>array('Galeri/index'),
);


?>

<!-- Formulir -->
<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'FormGaleri',
        'htmlOptions' => array('class' => 'well','enctype'=>'multipart/form-data'), // for inset effect
    )
);
echo $form->textFieldGroup($model, 'nama');
echo $form->fileFieldGroup($model,'file_galeri');
echo $form->html5EditorGroup(
			$model,
			'keterangan',
			 array(
				'widgetOptions' => array(
					'editorOptions' => array(
						'class' => 'span4',
						'rows' => 5,
						'height' => '200',
						'options' => array('color' => true),
						'language'=>'id',
					),
				)
				)
			);
echo $form->dropDownListGroup(
            $model,
            'status',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => array('1'=>'Tampil','0'=>'Draft'),
                )
            )
        );

$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);
 
$this->endWidget();
unset($form);
