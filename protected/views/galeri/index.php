<?php
Yii::app()->clientscript
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/flexslider.css')
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.flexslider.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/slider.js', CClientScript::POS_END)
?>
<?php
$this->title = "List Galeri";
$this->breadcrumbs = array(
    'Galeri' => array('index'),
);

$this->menu = array(
    'Active' => 'List Galeri',
    'List Galeri' => 'Galeri/index',
    'Tambah Galeri' => 'Galeri/tambah',
);
$galeri = Galeri::model()->findAll();
?>


<div class="row">
    <div class="col-lg-6">
        <div class="flexslider">
            <ul class="slides">
                <?php
                foreach (Galeri::model()->findAll() as $data):
                    echo "<li>";
                    echo "<div class='slide-desc'> $data->keterangan </div>";
                    echo "<img src=" . Galeri::model()->getUrlImage($data->id_galeri) . "/>";
                    echo "</li>";
                endforeach;

                ?>

            </ul>
        </div>
    </div>
    <div class="col-lg-6">

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'Galeri-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'cssFile'=>false,
            'summaryText' => '',
            'columns'=>array(
                'nama',
                array(
                    'header'=>'Keterangan',
                    'name'=>'keterangan',
                    'type'=>'html',

                ),
                array(
                    'header'=>'Status',
                    'name'=>'status',
                    'type'=>'html',
                    'value'=>
                        function($data){
                            if($data->status = 1):
                                $return = "<span class='blue'>Tampil</span>";
                            else:
                                $return = "<span class='red'>Draft</span>";
                            endif;
                            return $return;
                        }
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'update'=>array(
                            'label'=> 'Edit Galeri',
                            'url'=>'Yii::app()->createUrl("Galeri/ubah", array("id"=>$data->id_galeri))',
                        ),
                        'delete'=>array(
                            'label'=>'Hapus Galeri',
                            'url'=>'Yii::app()->createUrl("Galeri/hapus", array("id"=>$data->id_galeri))',
                        ),
                    )
                ),
            ),
        )); ?>
    </div>

</div>


