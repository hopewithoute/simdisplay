<?php

/**
 * This is the model class for table "video".
 *
 * The followings are the available columns in table 'video':
 * @property string $id_video
 * @property string $judul_video
 */
class Video extends CActiveRecord
{
	public $file_video;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul_video,url_video', 'length', 'max'=>255),
			array('file_video','file','types'=>'mp4,flv','allowEmpty'=>'true'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_video, judul_video,file_video,url_video', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_video' => 'Id Video',
			'judul_video' => 'Judul Video',
			'url_video'=>'Url Video',
		);
	}


	public function behaviors() {
		return array(
			'videoBehavior' => array(
				'class' => 'ext.clip.FileARBehavior',
				'attribute' => 'file_video', // this must exist
				'extension' => 'mp4,flv', // possible extensions, comma separated
				'prefix' => 'video_', // if you want a prefix
				'relativeWebRootFolder' => 'videos', // this folder must exist
				//'defaultName' => 'default', // you can also use a default file (see Ingredients with image below).
			)
		);	
	}

	public function getUrlVideo($id)
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition("id_video = $id");
		$model = Video::model()->find($criteria);


		if($model->url_video == ""):	
			$result = $model->videoBehavior->getFileUrl();
		else:
			$result = $model->url_video;
		endif;

		return $result;
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_video',$this->id_video,true);
		$criteria->compare('judul_video',$this->judul_video,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Video the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getOptions()
	{
		
		$model = $this->model()->findAll();

		return CHtml::listData($model,'id_video','judul_video');
	}
}
