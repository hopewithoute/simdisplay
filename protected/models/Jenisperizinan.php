<?php

/**
 * This is the model class for table "jenisperizinan".
 *
 * The followings are the available columns in table 'jenisperizinan':
 * @property string $id_jenisperizinan
 * @property string $nama_jenis
 * @property string $singkatan_jenis
 * @property integer $waktu_proses
 * @property string $persyaratan
 *
 * The followings are the available model relations:
 * @property Perizinan[] $perizinans
 */
class Jenisperizinan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenisperizinan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('waktu_proses', 'numerical', 'integerOnly'=>true),
			array('nama_jenis, singkatan_jenis', 'length', 'max'=>255),
			array('persyaratan,dihapus', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jenisperizinan,dihapus, nama_jenis, singkatan_jenis, waktu_proses, persyaratan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'perizinans' => array(self::HAS_MANY, 'Perizinan', 'id_jenisperizinan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jenisperizinan' => 'Id Jenisperizinan',
			'nama_jenis' => 'Nama Jenis Perizinan',
			'singkatan_jenis' => 'Singkatan Jenis Perizinan',
			'waktu_proses' => 'Waktu Proses',
			'persyaratan' => 'Persyaratan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jenisperizinan',$this->id_jenisperizinan,true);
		$criteria->compare('nama_jenis',$this->nama_jenis,true);
		$criteria->compare('singkatan_jenis',$this->singkatan_jenis,true);
		$criteria->compare('waktu_proses',$this->waktu_proses);
		$criteria->compare('persyaratan',$this->persyaratan,true);
		$criteria->compare('dihapus',0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jenisperizinan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getOptions()
	{
		
		$model = $this->model()->findAll();

		return CHtml::listData($model,'id_jenisperizinan','singkatan_jenis');
	}
}
