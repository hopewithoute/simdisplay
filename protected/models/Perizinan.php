<?php

/**
 * This is the model class for table "perizinan".
 *
 * The followings are the available columns in table 'perizinan':
 * @property string $id_perizinan
 * @property string $nama_pemohon
 * @property string $alamat
 * @property string $perusahaan
 * @property string $id_jenisperizinan
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Jenisperizinan $idJenisperizinan
 */
class Perizinan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'perizinan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('nama_pemohon, alamat, perusahaan, id_jenisperizinan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_perizinan, nama_pemohon, alamat, perusahaan, id_jenisperizinan, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisperizinan' => array(self::BELONGS_TO, 'Jenisperizinan', 'id_jenisperizinan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_perizinan' => 'Id Perizinan',
			'nama_pemohon' => 'Nama Pemohon',
			'alamat' => 'Alamat',
			'perusahaan' => 'Perusahaan',
			'id_jenisperizinan' => 'Id Jenisperizinan',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_perizinan',$this->id_perizinan,true);
		$criteria->compare('nama_pemohon',$this->nama_pemohon,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('perusahaan',$this->perusahaan,true);
		$criteria->compare('id_jenisperizinan',$this->id_jenisperizinan,true);		
		$criteria->compare('status',$this->status);		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                'defaultOrder'=>'id_perizinan DESC',
                'attributes'=>array(
                    '*'
                )

            ),
  		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Perizinan the static model class
	 */

	public function getProvider($status)
	{
		$criteria=new CDbCriteria;
		$criteria->limit = 5;

		if($status == 1):
			$criteria->compare('status',1);
		else:
			$criteria->compare('status',0);
		endif;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                'defaultOrder'=>'id_perizinan DESC',
                'attributes'=>array(
                    '*'
                )

            ),
            'pagination'=>false,
  		));

	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
