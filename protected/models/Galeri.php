<?php

/**
 * This is the model class for table "galeri".
 *
 * The followings are the available columns in table 'galeri':
 * @property string $id_galeri
 * @property string $nama
 * @property string $keterangan
 * @property integer $status
 */
class Galeri extends CActiveRecord
{
	public $file_galeri;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'galeri';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('nama, keterangan', 'length', 'max'=>255),
			array('file_galeri','file','types'=>'jpg,png,gif,jpeg','allowEmpty'=>'true'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_galeri, nama,file_galeri, keterangan, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_galeri' => 'Id Galeri',
			'nama' => 'Nama',
			'keterangan' => 'Keterangan',
			'status' => 'Status',
		);
	}

	public function behaviors() {
		  return array(
		    'imageBehavior' => array(
		      'class' => 'ext.clip.ImageARBehavior',
		      'attribute' => 'file_galeri', // this must exist
		      'extension' => 'png, gif, jpg,jpeg', // possible extensions, comma separated
		      'prefix' => 'img_',
		      'relativeWebRootFolder' => 'images', // this folder must exist	 
		    
		      'formats' => array(
		        // create a thumbnail grayscale format
		        'thumb' => array(
		        'suffix' => '_thumb',
		        'process' => array('resize' => array(60, 60), 'grayscale' => true),
		      ),
		      // create a large one (in fact, no resize is applied)
		        'large' => array(
		        'suffix' => '_large',
		      ),
		      // and override the default :
		        'normal' => array(
		      ),
		    ),
		 
		    'defaultName' => 'default', // when no file is associated, this one is used
		    // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
		    // and with one of the possible extensions. if multiple formats are used, a default file must exist
		    // for each format. Name is constructed like this :
		    //     {prefix}{name of the default file}{suffix}{one of the extension}
		  )
		);
	}
	public function getUrlImage($id)
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition("id_galeri = $id AND status= 1");
		$model = Galeri::model()->find($criteria);


		if($model->file_galeri !== ""):	
			$result = $model->imageBehavior->getFileUrl('normal');
		else:
			$result = "http://placehold.it/671x248";
		endif;

		return $result;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_galeri',$this->id_galeri,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Galeri the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
