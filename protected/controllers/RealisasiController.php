<?php

class RealisasiController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}
	public function accessRules(){
		return array(
				array('allow',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('@'),					
					),
					array('deny',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('?')
					),
				
				);
	}
	public function actionIndex()
	{
		$model = new Realisasi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Realisasi']))
			$model->attributes=$_GET['Realisasi'];

		$this->render('index',array('model'=>$model));
	}

	public function actionTambah()
	{

		$model = new Realisasi;
		if(isset($_POST['Realisasi'])):
			$model->attributes = $_POST['Realisasi'];
			$model->jumlah = Helper::cleanData($model->jumlah);
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Realisasi',
			'Data Realisasi'=>'Realisasi/index',
			'Tambah Realisasi'=>'Realisasi/tambah',
		);

		$this->render('form',array('model'=>$model));
	}

	public function actionUbah($id)
	{

		$model = $this->loadModel($id);
		if(isset($_POST['Realisasi'])):
			$model->attributes = $_POST['Realisasi'];
			$model->jumlah = Helper::cleanData($model->jumlah);
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Realisasi',
			'Data Realisasi'=>'Realisasi/index',
			'Tambah Realisasi'=>'Realisasi/tambah',
		);

		$this->render('form',array('model'=>$model));
	}

	public function actionHapus($id)
	{
        $model=$this->loadModel($id);       
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function loadModel($id)
	{

		$model=Realisasi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exists.');
		return $model;
	}



}