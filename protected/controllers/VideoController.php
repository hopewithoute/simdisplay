<?php

class VideoController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}
	public function accessRules(){
		return array(
				array('allow',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('@'),					
					),
					array('deny',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('?')
					),
				
				);
	}
	public function actionIndex()
	{
		$model = new Video('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Video']))
			$model->attributes=$_GET['Video'];

		$this->menu =array(
			'Active' => 'List Video',
			'List Video'=>'Video/index',
			'Tambah Video'=>'Video/tambah',
		);

		$this->render('index',array('model'=>$model));
	}

	public function actionTambah()
	{

		$model = new Video;
		if(isset($_POST['Video'])):
			$model->attributes = $_POST['Video'];
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Video',
			'Data Video'=>'Video/index',
			'Tambah Video'=>'Video/tambah',
		);

		$this->render('form',array('model'=>$model));
	}
	public function actionUbah($id)
	{

		$model = $this->loadModel($id);
		if(isset($_POST['Video'])):
			$model->attributes = $_POST['Video'];
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Video',
			'Data Video'=>'Video/index',
			'Tambah Video'=>'Video/tambah',
		);

		$this->render('form',array('model'=>$model));
	}

	public function actionHapus($id)
	{
        $model=$this->loadModel($id);       
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function actionDetail($id)
	{
		$model = $this->loadModel($id);

		$this->render('detail',array('model'=>$model));
	}
	public function loadModel($id)
	{

		$model=Video::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exists.');
		return $model;
	}
	
}