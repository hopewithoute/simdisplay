<?php

class ConfigController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}
	public function accessRules(){
		return array(
				array('allow',
						'actions'=>array('Index','LoadVideo'),
						'users'=>array('@'),					
					),
					array('deny',
						'actions'=>array('Index','LoadVideo'),
						'users'=>array('?')
					),
				
				);
	}
	
	public function actionIndex()
	{
		$model = Config::model()->find();
		if(isset($_POST['Config'])):
			$model->attributes = $_POST['Config'];
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;

		$this->render('form',array('model'=>$model));
	}

	
}