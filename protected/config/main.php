<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'BPMPPT - Display',
	'language'=>'id',
	'timeZone'=> 'Asia/Bangkok',
	'theme'=>'blackboot',

	// preloading 'log' component
	'preload'=>array('log','booster'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',	
		'ext.Select2.Select2',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'`',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),
	
	// application components
	'components'=>array(	
		'assetManager' => array(
            'linkAssets' => true,
        ),
        'formatter'=>array(
			'class'=>  'application.components.Formatter',
		),
		
		'user'=>array(
			'class'=>'WebUser',
			 'loginUrl'=>array('user/login'),
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'booster'=>array(
			'class'=>'ext.yiibooster.components.Booster',
			'bootstrapCss'=>true,
			'enableJS' => true,

		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<admin>'=>'user/login',
			),
		),
		
		
		/*'db' => array(
				'connectionString' => sprintf('mysql:host=%s;port=%d;dbname=%s', getenv('OPENSHIFT_MYSQL_DB_HOST'), getenv('OPENSHIFT_MYSQL_DB_PORT'), getenv('OPENSHIFT_APP_NAME')),
				'username' => getenv('OPENSHIFT_MYSQL_DB_USERNAME'),
				'password' => getenv('OPENSHIFT_MYSQL_DB_PASSWORD')
				//'schemaCachingDuration' => 3600,
			),	*/	
		'db' => array(
				'connectionString' => 'mysql:host=localhost;port=3306;dbname=bpmpptdisplay',
				'username' => 'root',
				'password' => '',
				//'schemaCachingDuration' => 3600,
			),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				/*array(
					'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
					// 'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);