<?php
	Yii::app()->clientscript
		// use it when you need it!
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/flat-ui.css' )
        ->registerCssFile(Yii::app()->theme->baseUrl . '/css/video-js.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/style.css' )
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/video.min.js', CClientScript::POS_END)
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/youtube.min.js', CClientScript::POS_END)
        //Insta
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap.js', CClientScript::POS_END )
		// ->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap-responsive.css' )		// ->registerCoreScript( 'jquery' )
        //Insta

		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-dropdown.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-scrollspy.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-tab.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-tooltip.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-popover.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-button.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-collapse.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-carousel.js', CClientScript::POS_END )
		// ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-typeahead.js', CClientScript::POS_END )
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BPMPPT- Display</title>
<meta name="language" content="en" />
</head>
</head>

<body>
	<?php
	$this->widget(
    'booster.widgets.TbNavbar',
    array(
        'type' => 'inverse',
        'brand' => CHtml::Image(Yii::app()->theme->baseUrl."/icon/favicon.ico",'',array('width'=>'24','height'=>'24')).' BPMPPT- Managemen Display',
        'brandUrl' => '#',
        'collapse' => true, // requires bootstrap-responsive.css
        'fixed' => false,
        'fluid' => true,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'items' => array(
                    array('label' => 'Video', 'url' => array('Video/index')),
                    array('label' => 'Slider', 'url' => array('Galeri/index')),
//                    array('label' => 'Realisasi', 'url' => array('Realisasi/index')),
//                    array(
//                        'label' => 'Izin',
//                        'items' => array(
//                            array('label' => 'Jenis Perijinan', 'url' => array('JenisPerizinan/Index')),
//                            array('label' => 'Status Perijinan','url' =>arraY('Perizinan/Index'))
//                        )
//                    ),
                    array('label' => 'Konfigurasi', 'url' => array('Config/index')),      
                ),
            ),
            
            array(
                    'class' => 'booster.widgets.TbMenu',
                    'type' => 'navbar',
                    'htmlOptions' => array('class' => 'pull-right'),
                    'items' => array(                   
                        array(
                            'label' => Helper::sesi('username'),
                            'url' => '#',
                            'items' => array(
                                array('label' => 'Manajemen User', 'url' => array('user/index'),'visible'=>!Yii::app()->user->isGuest),
                                array('label' => 'Logout', 'url' => array('user/logout'),'visible'=>!Yii::app()->user->isGuest),                           
                            )
                        ),
                    ),
                ),
            ),
    )
);?>
		<div class="container-fluid">
		 
		
		<?php echo $content ?>		
		
		</div>

</body>

</html>
