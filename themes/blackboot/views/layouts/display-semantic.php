<?php
//Reset
Yii::app()->clientScript->reset();

//Register As Needed
Yii::app()->clientScript
->registerCssFile( Yii::app()->theme->baseUrl . '/css/semantic.css' )
->registerScriptFile( Yii::app()->theme->baseUrl . '/js/jquery-2.1.1.js', CClientScript::POS_END )
->registerScriptFile( Yii::app()->theme->baseUrl . '/js/semantic.js', CClientScript::POS_END )

?>

<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title>BPMPPT - Display</title>

</head>
<body>
<div class="ui one column page grid">
	<div class="column">
<h4 class="ui inverted black block header">
  Black
</h4>	</div>
</div>
<div class="ui two column page grid black">
  <div class="column">
  	<div class="ui segment">
		<?php 
               $this->widget('ext.Yiippod.Yiippod', array(
                'video'=>Video::model()->getUrlVideo(Config::model()->find()->video_aktif), //if you don't use playlist
                //'video'=>"http://www.youtube.com/watch?v=qD2olIdUGd8", //if you use playlist
                'id' => 'yiippodplayer',
                'autoplay'=>false,
                'width'=>'100%',
                'view'=>6, 
                'height'=>'320',
                'bgcolor'=>'#000'
                ));
             ?>  	
  	</div>    
  </div>
  <div class="column">
    <div class="ui segment">
     <?php 
               $this->widget('ext.Yiippod.Yiippod', array(
                'video'=>Video::model()->getUrlVideo(Config::model()->find()->video_aktif), //if you don't use playlist
                //'video'=>"http://www.youtube.com/watch?v=qD2olIdUGd8", //if you use playlist
                'id' => 'yiippodplayer',
                'autoplay'=>false,
                'width'=>'100%',
                'view'=>6, 
                'height'=>'320',
                'bgcolor'=>'#000'
                ));
             ?>
  	</div>
   </div>
</div>
<div class="ui four column page grid">
	<div class="column">1</div>
	<div class="column">2</div>
	<div class="column">3</div>
	<div class="column">4</div>
</div>

<div class="ui two column page grid">
<div class="column"></div>
<div class="column"></div>
</div>

</body>
